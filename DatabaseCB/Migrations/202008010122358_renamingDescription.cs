namespace DatabaseCB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class renamingDescription : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Categories", "Description", c => c.String());
            AddColumn("dbo.Products", "Description", c => c.String());
            DropColumn("dbo.Categories", "Discription");
            DropColumn("dbo.Products", "Discription");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Products", "Discription", c => c.String());
            AddColumn("dbo.Categories", "Discription", c => c.String());
            DropColumn("dbo.Products", "Description");
            DropColumn("dbo.Categories", "Description");
        }
    }
}
