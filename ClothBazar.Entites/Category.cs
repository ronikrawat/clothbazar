﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothBazar.Entites
{
    public class Category : BaseEntities
    {
        public List<Product> Products { get; set; }
        public bool IsFeatured { get; set; }
    }
}
