﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothBazar.Entites
{
    public class Product : BaseEntities
    {

        public virtual Category Category { get; set; }
        public int CategoryID { get; set; }
        [Range(1,100000)]
        public decimal Price { get; set; }
    }
}
