﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothBazar.Entites
{
    public class BaseEntities
    {
        public int ID { get; set; }
        [Required]
        [MinLength(4),MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
        public string ImageURL { get; set; }

    }
}
