﻿using ClothBazar.Entites;
using ClothBazar.Web.ViewModels;
using ClothBazarServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace ClothBazar.Web.Controllers
{
   // [Authorize(Roles = "Admin")]
    public class CategoryController : Controller
    {
        public ActionResult Index()
        {
            var categories = CategoryService.Instance.GetAllCategories();
            return View(categories); 
        }
        public ActionResult CategoryTable(string search, int? pageNo)
        {
            CategorySearchViewModel model = new CategorySearchViewModel();
            model.SearchTerm = search;

            pageNo = pageNo.HasValue ? pageNo.Value > 0 ? pageNo.Value : 1 : 1;

            var totalRecords = CategoryService.Instance.GetCategoriesCount(search);
            model.Categories = CategoryService.Instance.GetCategories(search, pageNo.Value);

            if (model.Categories != null)
            {
                model.Pager = new Pager(totalRecords, pageNo,3);

                return PartialView("CategoryTable", model);
            }
            else
            {
                return HttpNotFound();
            }
        }
    
        [HttpGet]
        public ActionResult Create()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult Create(Category category)
        {
            if (ModelState.IsValid)
            {
                CategoryService.Instance.SaveCategory(category);
                return RedirectToAction("CategoryTable");
            }
            else
            {
                return new HttpStatusCodeResult(500);
            }
        }
        [HttpGet]
        public ActionResult Edit(int ID)
        {
            var category = CategoryService.Instance.GetCategory(ID);
            return PartialView(category);
        }
        [HttpPost]
        public ActionResult Edit(Category category)
        {
            if (category.ImageURL == null)
            {
                category.ImageURL = CategoryService.Instance.GetCategory(category.ID).ImageURL;
            }
            CategoryService.Instance.UpdateCategory(category);
            return RedirectToAction("CategoryTable");
        }
        [HttpPost]
        public ActionResult Delete(Category category)
        {
            CategoryService.Instance.DeleteCategory(category.ID);
            return RedirectToAction("CategoryTable");
        }
    }
}