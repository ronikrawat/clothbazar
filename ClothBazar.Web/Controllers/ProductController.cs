﻿using ClothBazar.Entites;
using ClothBazar.Web.ViewModels;
using ClothBazarServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClothBazar.Web.Controllers
{
    public class ProductController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ProductTable(string Search,int? pageNo)
        {
            ProductSearchViewModel model = new ProductSearchViewModel();
            model.PageNo = pageNo.HasValue ? pageNo.Value > 0 ? pageNo.Value : 1: 1;
            model.Products = ProductService.Instance.GetProducts(model.PageNo);
            model.Pager = new Pager(ProductService.Instance.GetProducts(), pageNo);
            if (string.IsNullOrEmpty(Search) == false)
            {
                model.SearchTerm = Search;
                model.Products = model.Products.Where(x => x.Name.ToLower().Contains(Search.ToLower())).ToList();
            }
            return PartialView(model);
        }
        [HttpGet]
        public ActionResult Create()
        {
            NewProductViewModel model = new NewProductViewModel();
            model.AvailableCategories = CategoryService.Instance.GetAllCategories();
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult Create(NewProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                var newProduct = new Product();
                newProduct.Name = model.Name;
                newProduct.Description = model.Description;
                newProduct.Price = model.Price;
                newProduct.Category = CategoryService.Instance.GetCategory(model.CategoryID);
                newProduct.ImageURL = model.ImageURL;
                ProductService.Instance.SaveProduct(newProduct);
                return RedirectToAction("ProductTable");
            }
            else
            {
                return new HttpStatusCodeResult(500);
            }
        }
        [HttpGet]
        public ActionResult Edit(int ID)
        {
            EditProductViewModel model = new EditProductViewModel();
            var product = ProductService.Instance.GetProduct(ID);
            model.ID = product.ID;
            model.Name = product.Name;
            model.Description = product.Description;
            model.Price = product.Price;
            model.CategoryID = product.Category != null ? product.Category.ID : 0;
            model.ImageURL = product.ImageURL;
            model.AvailableCategories = CategoryService.Instance.GetAllCategories();
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult Edit(EditProductViewModel model)
        {
            var product = ProductService.Instance.GetProduct(model.ID);
            product.Name = model.Name;
            product.Description = model.Description;
            product.Price = model.Price;
            product.Category = null;
            product.CategoryID = model.CategoryID;
            if(product.ImageURL == null)
            {
                product.ImageURL = ProductService.Instance.GetProduct(model.ID).ImageURL;
            }
            ProductService.Instance.UpdateProduct(product);
            return RedirectToAction("ProductTable");
        }
     
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            ProductService.Instance.DeleteProduct(ID);
            return RedirectToAction("ProductTable");
        }
        [HttpGet]
        public ActionResult Details(int id)
        {
            ProductSearchViewModel model = new ProductSearchViewModel();
            model.Product = ProductService.Instance.GetProduct(id);
            return View(model);
        }
    }
}