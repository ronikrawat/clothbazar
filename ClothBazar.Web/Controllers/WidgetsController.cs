﻿using ClothBazar.Web.ViewModels;
using ClothBazarServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClothBazar.Web.Controllers
{
    public class WidgetsController : Controller
    {
        // GET: Widgets
        public ActionResult Products(bool isLatestProducts, int? categoryId)
        {
            ProductWidgetViewModel model = new ProductWidgetViewModel();
            model.IsLatestProducts = isLatestProducts;
            if (isLatestProducts)
            {
                model.Products = ProductService.Instance.GetLatestProducts(4);
            }
            else if(categoryId.HasValue && categoryId.Value>0)
            {
                model.Products = ProductService.Instance.GetProductsByCategory(categoryId,4);
            }
            else
            {
                model.Products = ProductService.Instance.GetProducts(1, 8);

            }
            return PartialView(model);
        }
    }
}