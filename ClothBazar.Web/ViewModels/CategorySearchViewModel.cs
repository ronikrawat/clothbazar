﻿using ClothBazar.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClothBazar.Web.ViewModels
{
    public class CategorySearchViewModel
    {
        public int PageNo { get; set; }
        public string SearchTerm { get; set; }
        public List<Category> Categories { get; set; }
        public Pager Pager { get; set; }
    }
}