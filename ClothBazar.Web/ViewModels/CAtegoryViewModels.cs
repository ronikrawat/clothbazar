﻿using ClothBazar.Entites;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClothBazar.Web.ViewModels
{
    public class NewCategoryViewModel
    {
        public int id { get; set; }
        [Required]
        [MinLength(5),MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int CategoryID { get; set; }
        public string imageURL { get; set; }
        public List<Category> Categories { get; set; }
    }
}